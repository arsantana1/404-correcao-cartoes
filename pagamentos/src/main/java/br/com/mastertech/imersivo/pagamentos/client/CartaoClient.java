package br.com.mastertech.imersivo.pagamentos.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.mastertech.imersivo.pagamentos.dto.CartaoDTO;

@FeignClient(name = "cartao")
public interface CartaoClient {
	
	@GetMapping("/cartao/id/{id}")
	public CartaoDTO buscaCartaoPorId(@PathVariable Long id);

}
